import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgForm } from '@angular/forms';
import { ModalService } from '@universis/common';
import { GraduationWizardTabComponent } from '../../../graduation-wizard-tab/graduation-wizard-tab.component';
import {GraduationService} from '../../../../../../services/graduation-request/graduation.service';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'universis-graduation-document-submission',
  templateUrl: './graduation-document-submission.component.html'
})
export class GraduationDocumentSubmissionComponent extends GraduationWizardTabComponent implements OnInit, OnDestroy {

  @ViewChild('filename') filename;

  /**
   * Notifies that a file should be uploaded
   */
  @Output() uploadFile = new EventEmitter();

  /**
   * Notifies that a file should be uploaded
   */
  @Output() outputAction = new EventEmitter();

  /**
   * The file upload form
   */
  @ViewChild('studentGraduationRequestActionForm') studentGraduationRequestActionForm: NgForm;

  /**
   * The delete attachment modal template
   */
  @ViewChild('DeleteAttachmentModalTemplate') modalTemplate: TemplateRef<any>;

  /**
   * The delete attachment modal reference
   */
  modalRef;

  /**
   * The list of documents to show
   */
  public documents = [];

  /**
   * The list of document types for the student to submit
   */
  public attachmentTypes = [];

  /**
   * The available attachments
   */
  public attachmentsTypes = [];

  /**
   * The available attachments
   */
  public attachmentsPhysical = [];

  /**
   * A notice about the status of the documents submission
   */
  public statusNotice: string;

  /**
   * The list of the forms that the student can submit
   */
  public forms = [];

  /**
   * The id of the student action
   */
  public graduationRequestId: number;

  /**
   * The index of the document to be deleted
   */
  public documentIndex: number;

  /**
   *  The document that will be be deleted from the confirm delete modal
   */
  public deleteCandidate;

  public requestIsActive = true;

  public currentStep;

  constructor(
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _modalService: ModalService,
   // private _graduationService: GraduationRequestService
  ) {
    super();
    this.alternateName = 'graduationDocumentsSubmission';
   }

  public messageBlocksCollapseState: Array<boolean>;

  ngOnInit() {
    this.messageBlocksCollapseState = new Array(this.documents.length).fill(true);

    if (this.graduationRequestData && this.graduationRequestData.steps) {
      this.currentStep = this.graduationRequestData.steps.filter(step => {
        if (step && step.alternateName === 'graduationRequirementsCheck') {
          return step;
        }
      });
      this.fillData();
    } else {
      this.graduationRequestStatusObservable$.subscribe((graduationData) => {
        this.graduationRequestData = graduationData;
        this.currentStep = this.getStep(this.graduationRequestData);
        this.fillData();
      });
    }
  }

  fillData() {
     // tslint:disable-next-line:max-line-length
    this.requestIsActive = this.graduationRequestData.graduationRequest.actionStatus.alternateName === 'PotentialActionStatus' ? true : false;

    if (this.currentStep.status) {
      this.statusNotice = this._translateService.instant(
        `UniversisGraduationModule.DocumentsSubmission.SubmissionStatuses.${this.currentStep.status}`
      );
    }

    if (this.currentStep && this.graduationRequestData) {
      this.graduationRequestId = this.graduationRequestData.graduationRequest.id;

      if (this.graduationRequestData.graduationRequest.graduationEvent.attachmentTypes) {
        this.attachmentsTypes = this.graduationRequestData.graduationRequest.graduationEvent.attachmentTypes;

        // const requestDocumentsStatus = Object.assign(this.attachmentsTypes, data.graduationRequest.attachments);
        const requestDocumentsStatus = this.attachmentsTypes.map((a) => {
          const obj2 = this.graduationRequestData.graduationRequest.attachments.find((b) => a.attachmentType.id === b.attachmentType.id);
          if (obj2) {
            Object.assign(a, obj2);
          }
          return a;
        });

        this.attachmentsPhysical = this.attachmentsTypes.filter((attachment) =>
          attachment.attachmentType && attachment.attachmentType.physical === true
        );

        this.attachmentsTypes = this.attachmentsTypes.filter((attachment) =>
          !attachment.attachmentType.physical
        );
      }
    }
  }

  ngOnDestroy() {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

  /**
   *
   * Triggers a file download from the graduation request
   *
   * @param attachmentTypeIndex the index of the attachment to download
   *
   */
  public async downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  /**
   *
   * Emits a file upload event
   *
   * @param attachmentTypeIndex The index of the attachment type
   *
   */
  public async triggerUploadFile(attachmentTypeIndex: number) {
    if (this.attachmentsTypes[attachmentTypeIndex].file) {
      this.outputAction.emit({
        action: 'upload',
        data: {
          graduationRequestId: this.graduationRequestId,
          file: this.attachmentsTypes[attachmentTypeIndex].file,
          attachmentType: this.attachmentsTypes[attachmentTypeIndex].attachmentType.id
        }
      });
    }
  }

  /**
   *
   * Emits a file remove event
   *
   * @param attachmentTypeIndex
   *
   */
  public async triggerRemoveFile(attachmentTypeIndex: number) {
    if (this.attachmentsTypes[attachmentTypeIndex].name) {
     this.outputAction.emit({
        action: 'remove',
        data: {
          requestId: this.graduationRequestId,
          attachmentId: this.attachmentsTypes[attachmentTypeIndex].id,
          attachmentTypeAlternateName: this.attachmentsTypes[attachmentTypeIndex].attachmentType.id
        }
      });
    } else {
      console.warn('Graduation document remove attempt failed');
    }
  }

  /**
   *
   * Event handler for the file change event
   *
   * @param event The event as produced by the browser
   * @param attachmentTypeIndex The index of the attachment type
   *
   */
  onFileChange(event, attachmentTypeIndex) {
    const selectedFile = (event.target as HTMLInputElement).files[0];
    this.attachmentsTypes[attachmentTypeIndex].file = selectedFile;
    this.attachmentsTypes[attachmentTypeIndex].name = selectedFile.name;
  }

  /**
   *
   * Opens the delete modal and set the current document index
   *
   * @param attachmentIndex The index of the current attachment (request attachment)
   *
   */
  openDeleteModal(attachmentIndex: number) {
    if (typeof attachmentIndex === 'number') {
      this.deleteCandidate = {
        index: attachmentIndex,
        type: this.attachmentsTypes[attachmentIndex].name
      };
      this.modalRef = this._modalService.openModal(this.modalTemplate);
    } else {
      console.warn('Attempt to delete document without index');
    }
  }

  /**
   *
   * Close the modal without any other action
   *
   */
  closeDeleteModal() {
    this.modalRef.hide();
  }

  /**
   *
   * Calls the file removal for the current document
   *
   */
  confirmDocumentDelete() {
    this.triggerRemoveFile(this.deleteCandidate.index);
    this.closeDeleteModal();
  }
}
