import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationWizardTabComponent } from './graduation-wizard-tab.component';

describe('GraduationWizardTabComponent', () => {
  let component: GraduationWizardTabComponent;
  let fixture: ComponentFixture<GraduationWizardTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationWizardTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationWizardTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
