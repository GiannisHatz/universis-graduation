/* tslint:disable max-line-length */
export const el = {
  UniversisGraduationModule: {
    Degree: 'Πτυχίο',
    DegreePrerequisites: 'Προυποθέσεις Πτυχίου',
    Graduation: 'Ορκωμοσία',
    GraduationRequestWizard: 'Οδηγός αίτησης ορκωμοσίας',
    GraduationRequestOverview: 'Προεπισκόπηση αίτησης ορκωμοσίας',
    BackToDegree: 'Πίσω στο πτυχίο',
    GraduationDatesNotice: 'Η περίοδος αιτήσεων ορκωμοσίας θα είναι ανοιχτή από {{dateStart}} εώς {{dateEnd}}.',
    WithoutGraduationRequestMessage: 'Η περίοδος αιτήσεων ορκωμοσίας για το {{semesterPeriod}} Εξάμηνο {{academicYear}} είναι ανοιχτή. Μπορείς να μπεις στον οδηγό αίτησης ορκωμοσίας για να κάνεις αίτηση.',
    PotentialGraduationRequestMessage: 'H περίοδος αιτήσεων ορκωμοσίας για το {{semesterPeriod}} Εξάμηνο {{academicYear}} είναι ανοιχτή. Μπορείς να διαχειριστείς την αίτηση σου.',
    ActiveGraduationRequestMessage: 'Η αίτηση ορκωμοσίας σου για το {{semesterPeriod}} Εξάμηνο {{academicYear}} είναι σε αναμονή για έλεγχο.',
    CompletedGraduationRequestMessage: 'Η αίτηση ορκωμοσίας σου για το {{semesterPeriod}} Εξάμηνο {{academicYear}} έχει γίνει αποδεκτή.',
    WithoutGraduationEventMessage: 'Αυτή τη στιγμή δεν είναι διαθέσιμη η δυνατότητα αίτησης ορκωμοσίας στο τμήμα σου.',
    Semester: {
      title: 'Εξάμηνο',
      summer: 'εαρινό',
      winter: 'χειμερινό',
      summerPossessive: 'εαρινού',
      winterPossessive: 'χειμερινού',
      caps: {
        title: 'ΕΞΑΜΗΝΟ',
        winter: 'ΧΕΙΜΕΡΙΝΟ',
        summer: 'ΕΑΡΙΝΟ'
      }
    },
    GraduationRequestTitle: 'Αίτηση ορκωμοσίας',
    GraduationRequestCapitalTitle: 'ΑΙΤΗΣΗ ΟΡΚΩΜΟΣΙΑΣ',
    SpecialRequest: {
      Title: 'Ειδικό αίτημα',
      Notice: 'Αν χρειάζεσαι να κάνεις ειδικό αίτημα, γράψε το μήνυμα σου στο παρακάτω πεδίο ώστε να το δει η γραμματεία σου.',
      InputPlaceholder: 'Το μήνυμά σου για την γραμματεία'
    },
    GraduationRequest: {
      Title: 'Αίτηση ορκωμοσίας',
      DefaultRequestName: 'Αιτούμαι να υποβάλω αίτηση αποφοίτησης για το {{academicPeriod}} εξάμηνο {{academicYear}}.',
      ParticipateInCeremony: 'Συμφωνώ ότι θα παρευρεθώ στην εκδήλωση ορκωμοσίας του {{academicPeriod}} εξαμήνου.',
      RequirementsChecked: 'Έχω ελέγξει ότι πληρούνται οι προϋποθέσεις για να αποφοιτήσω.',
      WillAnnounceDatesNotice: 'Η ημερομηνία και η τοποθεσία της ορκωμοσίας θα ανακοινωθούν αμέσως μετά την λήξη των αιτήσεων ορκωμοσίας.',
      Request: 'Αίτηση Ορκωμοσίας',
      errors: {
        generic: 'Υπήρξε σφάλμα κατά την υποβολή της αίτησης',
        EUNQ: 'Έχετε ήδη υποβάλει αίτηση ορκωμοσίας για την τρέχουσα περίοδο ορκωμοσίας'
      }
    },
    RequirementsCheck: {
      Title: 'Έλεγχος Προϋποθέσεων',
      Subtitle: 'Για να ολοκληρώσεις την Αίτηση Ορκωμοσίας σου, θα πρέπει να πληρούνται οι απαραίτητες προϋποθέσεις',
      StatusCheck: 'Κατάσταση Ελέγχου',
      DegreeRequirements: 'Προϋποθέσεις πτυχίου',
      Status: {
        unavailable: 'Ο έλεγχος για τις προϋποθέσεις πτυχίου δεν έχει ξεκινήσει',
        failed: 'Οι προϋποθέσεις πτυχίου δεν πληρούνται',
        pending: 'Γίνεται έλεγχος των προϋποθέσεων πτυχίου',
        completed: 'Ο έλεγχος για τις προϋποθέσεις πτυχίου ολοκληρώθηκε επιτυχώς'
      }
    },
    DocumentsSubmission: {
      Title: 'Κατάθεση εγγράφων',
      Subtitle: 'Ακολούθησε τις παρακάτω οδηγίες για να καταθέσεις τα απαιτούμενα έγγραφα για την ορκωμοσία σου.',
      SubmissionStatus: 'Κατάσταση κατάθεσης',
      SubmissionStatuses: {
        pending: 'Η κατάθεση εγγράφων για την ορκωμοσία είναι σε εξέλιξη',
        completed: 'Η κατάθεση εγγράφων για την ορκωμοσία έχει ολοκληρωθεί',
        failed: 'Η κατάθεση εγγράφων για την ορκωμοσία είναι σε εξέλιξη',
        unavailable: 'Η κατάθεση εγγράφων δεν είναι διαθέσιμη'
      },
      AttachmentDeleteModal: {
        Title: 'Διαγραφή εγγράφου',
        Body: 'Διαγραφή εγγράφου τύπου {{attachmentType}};',
        Notice: 'Αυτή η πράξη δεν είναι αναιρέσιμη.',
        Close: 'Κλείσιμο',
        Delete: 'Διαγραφή'
      },
      GraduationDocumentsToUpload: 'Έγγραφα ορκωμοσίας για μεταφόρτωση',
      GraduationDocumentsPhysicals: 'Έγγραφα ορκωμοσίας που πρέπει να παραδοθούν στην γραμματεία του τμήματος',
      DownloadDocument: 'Λήψη εγγράφου',
      UploadDocument: 'Μεταφόρτωση',
      RemoveDocument: 'Αφαίρεση αρχείου',
      ContactService: 'Επικοινωνία με υπεύθυνο',
      Errors: {
        Download: 'Υπήρξε σφάλμα κατά την λήψη του αρχείου',
        Remove: 'Υπήρξε σφάλμα κατά την αφαίρεση του αρχείου',
        Upload: 'Υπήρξε σφάλμα κατά την μεταφόρτωση του αρχείου'
      }
    },
    GraduationCeremony: {
      Title: 'Οριστική Υποβολή Αίτησης Ορκωμοσίας',
      ParticipationStatus: 'Κατάσταση συμμετοχής',
      GraduationCeremonyStatus: 'Στοιχεία εκδήλωσης ορκωμοσίας',
      GraduationCertificates: 'Πιστοποιητικά ορκωμοσίας',
      ContactRegistrar: 'Επικοινωνία με την γραμματεία',
      MandatoryParticipation: 'Η προσέλευσή σου στην ορκωμοσία του τμήματος είναι υποχρεωτική.'
    },
    ModalConfirm: {
      Submit: 'Ολοκλήρωση',
      Close: 'Κλείσιμο',
      Title: 'Αποστολή Αίτησης Ορκωμοσίας',
      Body: 'Θέλετε να στείλετε την αίτηση σας για αποστολή και έλεγχο στην γραμματεία;'
    },
    MessagePrompt: 'Το μήνυμά σου',
    Send: 'Αποστολή',
    Cancel: 'Ακύρωση',
    Date: 'Ημερομηνία',
    Time: 'Ώρα',
    Location: 'Τοποθεσία',
    Url: 'Σχετικό URL',
    Download: 'Λήψη',
    Previous:  'Προηγούμενο',
    Next: 'Επόμενο',
    Completed: 'Ολοκλήρωση Αίτησης',
    GraduationRules: 'Προϋποθέσεις Πτυχίου',
    GraduationApply: 'Αίτηση για Ορκωμοσία',
    ComplexRules: 'Το πρόγραμμα σπουδών που ακολουθείς έχει σύνθετους κανόνες αποφοίτησης',
    ComplexRulesSuccess: 'Πληρούνται οι προυποθέσεις πτυχίου',
    ComplexRulesNoSuccess: 'Δεν πληρούνται οι προυποθέσεις πτυχίου',
    ContactRegistrar: 'Επικοινωνία με Γραμματεία',
    GraduationInfo: 'Για περισσότερες πληροφορίες σχετικά με τις προϋποθέσεις πτυχίου μπορείτε να δείτε τον οδηγό σπουδών ή να επικοινωνήσετε με την Γραμματεία.',
    StudentInfo: 'Στοιχεία Φοιτητή',
    StudyGuide: 'ΟΔΗΓΟΣ ΣΠΟΥΔΩΝ',
    Specialty : 'ΚΑΤΕΥΘΥΝΣΗ',
    Prerequisites: 'Προϋποθέσεις',
    Progress: 'Πρόοδος',
    NoRulesFound: 'Οι προϋποθέσεις πτυχίου δεν έχουν οριστεί.',
    CourseType: 'Τύπος Μαθημάτων',
    AllTypeCourses: 'Όλοι οι τύποι μαθημάτων',
    Thesis: 'Εργασία',
    Student: 'Ιδιότητες Φοιτητή',
    Internship: 'Πρακτική',
    Course: 'Προαπαιτούμενο Μάθημα',
    CourseArea: 'Γνωστικό Αντικείμενο',
    CourseCategory: 'Κατηγορία Μαθήματος',
    CourseSector: 'Τομέας Μαθημάτων',
    ProgramGroup: 'Ομάδα Μαθημάτων',
    StatusLabel: 'Κατάσταση της αίτησης σου',
    NoAttachments: 'Δεν υπαρχουν έγγραφα για μεταφόρτωση'
  }
};
